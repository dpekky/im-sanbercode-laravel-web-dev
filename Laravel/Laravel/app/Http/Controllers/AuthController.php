<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view ('register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];
        return view('welcome', compact('namaDepan', 'namaBelakang'));
        // return view('welcome')->with(['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
        // return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
