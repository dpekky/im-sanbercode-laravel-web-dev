<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <label>First Name :</label>
            <br><br>
            <input type="text" name="firstName">
            <br><br>
            <label>Last Name :</label>
            <br><br>
            <input type="text" name="lastName">
            <br>
            <p>Gender :</p>
            <input type="radio" id="radio" name="jeniskelamin" value="Male"><label for="radio">Male</label>
            <br>
            <input type="radio" id="radio" name="jeniskelamin" value="female"><label for="radio">Female</label>
            <br>
            <input type="radio" id="radio" name="jeniskelamin" value="other"><label for="radio">Other</label>
            <br>
            <p>Nationality :</p>
            <select>
                <option>Indonesia</option>
                <option>Singapore</option>
                <option>Malaysia</option>
                <option>Australia</option>
            </select>
            <br>
            <p>Language Spoken :</p>
            <input type="checkbox" name="language[]" value="Bahasa Indonesia"><label for="bindo">Bahasa Indonesia </label>
            <br>
            <input type="checkbox" name="language[]" value="English"><label for="english">English</label>
            <br>
            <input type="checkbox" name="language[]" value="ohther"><label for="other">Other</label>
            <p>Bio :</p>
            <textarea name="" id="" cols="21" rows="5"></textarea>
            <br>
            <button type="submit">Sign Up</button>
        </form>
    </body>
</html>