<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");

echo " Animal Name : " . $sheep->name . "<br>"; // "shaun"
echo " Legs : " . $sheep->legs . "<br>"; // 4
echo " Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new frog("buduk");
echo " Animal Name : " . $kodok->name . "<br>"; // "shaun"
echo " Legs : " . $kodok->legs . "<br>"; // 4
echo " Cold Blooded : " . $kodok->cold_blooded . "<br>"; // "no
echo "Jump : " . $kodok->jump() . "<br><br>";// "hop hop"

$sungokong = new ape("kera sakti");
echo " Animal Name: " . $sungokong->name . "<br>"; // "shaun"
echo " Legs : " . $sungokong->legs . "<br>"; // 4
echo " Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : " . $sungokong->yell(); // "Auooo"

?>