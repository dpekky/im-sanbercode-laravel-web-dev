<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama Tidak Boleh Kosong',
            'umur.required' => 'Umur Tidak Boleh Kosong',
            'bio.required' => 'Biodata Tidak Boleh Kosong'
        ]);
        
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('cast.tampil', compact('cast'));
    }

    public function show($id)
    {
        $castShow = DB::table('cast')->find($id);
        // dd($castShow);
        return view('cast.detail', compact('castShow'));
    }

    public function edit($id)
    {
        $castShow = DB::table('cast')->find($id);
        return view('cast.edit', compact('castShow'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ],
        [
            'nama.required' => 'Nama Tidak Boleh Kosong',
            'umur.required' => 'Umur Tidak Boleh Kosong',
            'bio.required' => 'Biodata Tidak Boleh Kosong'
        ]);
        DB::table('cast')
            ->where('id', $id)
            ->update(
            [
            'nama' => $request ['nama'],
            'umur' => $request ['umur'],
            'bio' => $request ['bio']
            ]
        );
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
