@extends('app')
@section('content')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Halaman Lengkap Biodata</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/cast/tambah')}}">Cast</a></li>
            <li class="breadcrumb-item active">Bio</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Detail Biodata</h3>
        </div>
        <!-- /.card-header -->
        @csrf
        <div class="card-body">
            <table class="table">
                <h1>{{$castShow->nama}}</h1>
                <p>{{$castShow->bio}}</p>
                <a href="/cast" class="btn btn-info btn-sm">Kembali</a>
            </table>
        </div>
    </div>
</section>
@endsection('content')