@extends('app')
@section('content')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Halaman Tampil Cast</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/cast')}}">Cast</a></li>
            <li class="breadcrumb-item active">View</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Daftar Cast</h3>
        </div>
        <!-- /.card-header -->
        @csrf
        <div class="card-body">
            <a href="{{url('/cast/tambah')}}" class="btn btn-primary">Tambah Cast</a>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Biodata</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=> $cast)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$cast->nama}}</td>
                            <td>{{$cast->umur}}</td>
                            <td>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                                    <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>Data Cast Kosong</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection('content')