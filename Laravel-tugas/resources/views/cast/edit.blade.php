@extends('app')
@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Halaman Edit Data Cast</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/cast')}}">Cast</a></li>
            <li class="breadcrumb-item active">Edit</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Form Edit Cast</h3>
        </div>
        <!-- /.card-header -->
        <form action="/cast/{{$castShow->id}}" method="POST">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{$castShow->nama}}" placeholder="Masukkan Nama">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" name="umur" value="{{$castShow->umur}}" placeholder="Masukkan Umur">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Biodata</label>
                    <textarea class="form-control" name="bio" rows="3" placeholder="Tuliskan Biodata">{{$castShow->bio}}</textarea>
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
      </div>
    </div>
</section>
@endsection('content')