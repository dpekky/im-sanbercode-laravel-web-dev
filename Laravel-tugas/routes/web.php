<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'home']);
Route::get('/table', [TableController::class, 'index']);
Route::get('/data-tables', [DataTableController::class, 'index']);




//CRUD
//Create
//Form tambah cast
Route::get('/cast/tambah', [CastController::class, 'create']);
//Simpan data ke tabel cast
Route::post('/cast', [CastController::class,'store']);
//Read data
Route::get('/cast', [CastController::class,'index']);
// //Rote detail biodata
Route::get('/cast/{cast_id}', [CastController::class,'show']);
//Update Data
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);
//Edit data berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class,'update']);
//Delete
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);